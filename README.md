# Section 1 Android Marathon

Task "Introduction" for VibeLab

1. [Hello, world!](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/Hello,%20world)
1. [Named arguments](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/Named%20arguments)
1. [Default arguments](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/Default%20arguments)
1. [Triple-quoted strings](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/Triple-quoted%20strings)
1. [String templates](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/StringTemplates)
1. [Nullable types](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/NullableTypes)
1. [Nothing type](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/NothingType)
1. [Lambdas](https://gitlab.com/chd-k/section-1-android-marathon/-/tree/main/Lambdas)
